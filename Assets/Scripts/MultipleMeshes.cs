using System;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class MultipleMeshes : MonoBehaviour
{
    private static readonly int BaseColorId = Shader.PropertyToID("_BaseColor");
    private static readonly int MetallicId = Shader.PropertyToID("_Metallic");
    private static readonly int SmoothnessId = Shader.PropertyToID("_Smoothness");

    [SerializeField] private int _count = 1023;
    [SerializeField] private Mesh _mesh;
    [SerializeField] private Material _material;
    [SerializeField] private Vector3 _volumeSize;
    [SerializeField] private LightProbeProxyVolume _lightProbeProxyVolume;

    private Matrix4x4[] _matrices;
    private Vector4[] _baseColors;
    private float[] _metallics;
    private float[] _smoothnesses;
    private MaterialPropertyBlock _materialPropertyBlock;

    private void OnValidate()
    {
        _matrices = new Matrix4x4[_count];
        _baseColors = new Vector4[_count];
        _metallics = new float[_count];
        _smoothnesses = new float[_count];

        var pos = transform.position;
        var max = _volumeSize * 0.5f;
        var min = -max;
        for (var i = 0; i < _count; i++)
        {
            var iPos = pos + new Vector3(
                Random.Range(min.x, max.x),
                Random.Range(min.y, max.y),
                Random.Range(min.z, max.z));
            var iRot = Quaternion.Euler(
                Random.Range(0f, 360f),
                Random.Range(0f, 360f),
                Random.Range(0f, 360f));
            _matrices[i] = Matrix4x4.TRS(iPos, iRot, Vector3.one * Random.Range(0.7f, 1.3f));
            _baseColors[i] = new Vector4(Random.value, Random.value, Random.value, Random.Range(0.6f, 1f));
            _metallics[i] = Random.value < 0.25f ? 1f : 0f;
            _smoothnesses[i] = Random.Range(0.05f, 0.95f);
        }

        _positions = new Vector3[_count];
        _lightProbes = new SphericalHarmonicsL2[_count];
    }

    private Vector3[] _positions;
    private SphericalHarmonicsL2[] _lightProbes;

    private void Update()
    {
        if (_material == null || _mesh == null)
            return;
        if (_materialPropertyBlock == null)
        {
            _materialPropertyBlock = new MaterialPropertyBlock();
            _materialPropertyBlock.SetVectorArray(BaseColorId, _baseColors);
            _materialPropertyBlock.SetFloatArray(MetallicId, _metallics);
            _materialPropertyBlock.SetFloatArray(SmoothnessId, _smoothnesses);
        }

        if (_lightProbeProxyVolume == null)
        {
            for (var i = 0; i < _count; i++)
            {
                _positions[i] = _matrices[i].GetColumn(3);
            }
            LightProbes.CalculateInterpolatedLightAndOcclusionProbes(_positions, _lightProbes, null);
            _materialPropertyBlock.CopySHCoefficientArraysFrom(_lightProbes);
        }

        Graphics.DrawMeshInstanced(_mesh, 0, _material, _matrices, _count, _materialPropertyBlock,
            ShadowCastingMode.On, true, 0, null,
            _lightProbeProxyVolume != null ? LightProbeUsage.UseProxyVolume : LightProbeUsage.CustomProvided,
            _lightProbeProxyVolume);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position, _volumeSize);
    }
}