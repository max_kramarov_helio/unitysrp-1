using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public class Shadows
    {
        private struct ShadowedDirectionalLight
        {
            public int visibleLightIndex;
            public float slopeScaleBias;
            public float nearPlaneOffset;
        }

        private const string BufferName = "Shadows";
        private const int MaxShadowedDirectionalLightCount = 4;
        private const int MaxCascades = 4;
        private static readonly int DirectionalShadowAtlasId = Shader.PropertyToID("_DirectionalShadowAtlas");
        private static readonly int DirectionalShadowMatrices = Shader.PropertyToID("_DirectionalShadowMatrices");
        private static readonly int CascadeCountId = Shader.PropertyToID("_CascadeCount");
        private static readonly int CascadeCullingSpheresId = Shader.PropertyToID("_CascadeCullingSpheres");
        private static readonly int CascadeDataId = Shader.PropertyToID("_CascadeData");
        private static readonly int ShadowDistanceFadeId = Shader.PropertyToID("_ShadowDistanceFade");
        private static readonly int ShadowAtlasSizeId = Shader.PropertyToID("_ShadowAtlasSizeId");
        private static readonly string[] DirectionalFilterKeywords = new string[]
        {
            "_DIRECTIONAL_PCF3",
            "_DIRECTIONAL_PCF5",
            "_DIRECTIONAL_PCF7",
        };
        private static readonly string[] CascadeBlendModeKeywords = new string[]
        {
            "_CASCADE_BLEND_SOFT",
            "_CASCADE_BLEND_DITHER",
        };

        private readonly CommandBuffer _commandBuffer;
        private readonly ShadowedDirectionalLight[] _shadowedDirectionalLights;
        private readonly Vector4[] _cascadeCullingSpheres;
        private readonly Vector4[] _cascadeData;
        private readonly CustomRenderPipelineSettings _settings;
        private readonly Matrix4x4[] _dirShadowMatrices;
        private int _shadowedDirectionalLightCount;

        private ScriptableRenderContext _context;
        private CullingResults _cullingResults;

        public Shadows(CustomRenderPipelineSettings settings)
        {
            _commandBuffer = new CommandBuffer()
            {
                name = BufferName,
            };
            _shadowedDirectionalLights = new ShadowedDirectionalLight[MaxShadowedDirectionalLightCount];
            _cascadeCullingSpheres = new Vector4[MaxCascades];
            _cascadeData = new Vector4[MaxCascades];
            _settings = settings;
            _dirShadowMatrices = new Matrix4x4[MaxShadowedDirectionalLightCount * MaxCascades];
        }

        public void Setup(ScriptableRenderContext context, CullingResults cullingResults)
        {
            _context = context;
            _cullingResults = cullingResults;

            _commandBuffer.BeginSample(BufferName);
            ExecuteCommandBuffer();

            SetupShadows();

            _commandBuffer.EndSample(BufferName);
            ExecuteCommandBuffer();
        }

        private void SetupShadows()
        {
            _shadowedDirectionalLightCount = 0;
        }

        public Vector3 TryReserveDirectionalShadows(Light light, int visibleLightIndex)
        {
            if (_shadowedDirectionalLightCount < MaxShadowedDirectionalLightCount
                && light.shadows != LightShadows.None && light.shadowStrength > 0f
                && _cullingResults.GetShadowCasterBounds(visibleLightIndex, out var bounds))
            {
                _shadowedDirectionalLights[_shadowedDirectionalLightCount] = new ShadowedDirectionalLight()
                {
                    visibleLightIndex = visibleLightIndex,
                    slopeScaleBias = light.shadowBias,
                    nearPlaneOffset = light.shadowNearPlane,
                };
                return new Vector3(
                    light.shadowStrength,
                    _settings.shadowSettings.directional.cascadeCount * _shadowedDirectionalLightCount++,
                    light.shadowNormalBias);
            }
            return Vector2.zero;
        }

        public void Render()
        {
            if (_shadowedDirectionalLightCount > 0)
                RenderDirectionalShadows();
        }

        private void RenderDirectionalShadows()
        {
            _commandBuffer.BeginSample(BufferName);

            var atlasSize = (int)_settings.shadowSettings.directional.textureSize;
            _commandBuffer.GetTemporaryRT(DirectionalShadowAtlasId, atlasSize, atlasSize, 32,
                FilterMode.Bilinear, RenderTextureFormat.Shadowmap);
            _commandBuffer.SetRenderTarget(DirectionalShadowAtlasId, RenderBufferLoadAction.DontCare,
                RenderBufferStoreAction.Store);
            _commandBuffer.ClearRenderTarget(true, false, Color.clear);

            var tiles = _shadowedDirectionalLightCount * _settings.shadowSettings.directional.cascadeCount;
            var split = 1;
            if (tiles <= 1)
                split = 1;
            else if (tiles <= 4)
                split = 2;
            else
                split = 4;
            var tileSize = atlasSize / split;
            for (var i = 0; i < _shadowedDirectionalLightCount; i++)
            {
                RenderDirectionalShadows(i, split, tileSize);
            }

            var shadowSettings = _settings.shadowSettings;
            _commandBuffer.SetGlobalMatrixArray(DirectionalShadowMatrices, _dirShadowMatrices);
            _commandBuffer.SetGlobalVectorArray(CascadeCullingSpheresId, _cascadeCullingSpheres);
            _commandBuffer.SetGlobalVectorArray(CascadeDataId, _cascadeData);
            _commandBuffer.SetGlobalInt(CascadeCountId, shadowSettings.directional.cascadeCount);
            var f = 1f - shadowSettings.directional.cascadeFade;
            _commandBuffer.SetGlobalVector(ShadowDistanceFadeId, new Vector4(
                1f / shadowSettings.maxDistance,
                1f / shadowSettings.distanceFade,
                1f / (1f - f * f)));
            _commandBuffer.SetGlobalVector(ShadowAtlasSizeId, new Vector4(atlasSize, 1f / atlasSize));

            SetKeywords(DirectionalFilterKeywords, (int)shadowSettings.directional.filter - 1);
            SetKeywords(CascadeBlendModeKeywords, (int)shadowSettings.directional.cascadeBlend - 1);
            _commandBuffer.EndSample(BufferName);
            ExecuteCommandBuffer();
        }

        private void RenderDirectionalShadows(int index, int split, int tileSize)
        {
            var light = _shadowedDirectionalLights[index];
            var shadowSettings = new ShadowDrawingSettings(_cullingResults, light.visibleLightIndex);
            var cascadeCount = _settings.shadowSettings.directional.cascadeCount;
            var cascadeRatios = _settings.shadowSettings.directional.CascadeRatios;
            var cullingFactor = 0.8f - _settings.shadowSettings.directional.cascadeFade;
            for (var i = 0; i < cascadeCount; i++)
            {
                _cullingResults.ComputeDirectionalShadowMatricesAndCullingPrimitives(
                    light.visibleLightIndex, i, cascadeCount, cascadeRatios, tileSize,
                    light.nearPlaneOffset, out var viewMatrix, out var projMatrix, out var shadowSplitData);
                shadowSplitData.shadowCascadeBlendCullingFactor = cullingFactor;
                shadowSettings.splitData = shadowSplitData;
                if (index == 0)
                {
                    SetCascadeData(i, shadowSplitData.cullingSphere, tileSize);
                }
                var tileIndex = index * cascadeCount + i;
                var tileOffset = SetTileViewPort(tileIndex, split, tileSize);
                _commandBuffer.SetViewProjectionMatrices(viewMatrix, projMatrix);
                _dirShadowMatrices[tileIndex] = ConvertToAtlasMatrix(projMatrix * viewMatrix, tileOffset, split);

                _commandBuffer.SetGlobalDepthBias(0f, light.slopeScaleBias);
                ExecuteCommandBuffer();
                _context.DrawShadows(ref shadowSettings);
                _commandBuffer.SetGlobalDepthBias(0f, 0f);
            }
        }

        private Vector2 SetTileViewPort(int index, int split, float tileSize)
        {
            var offset = new Vector2(index % split, index / split);
            var vp = new Rect(offset.x * tileSize, offset.y * tileSize, tileSize, tileSize);
            _commandBuffer.SetViewport(vp);
            return offset;
        }

        private Matrix4x4 ConvertToAtlasMatrix(Matrix4x4 m, Vector2 offset, int split)
        {
            if (SystemInfo.usesReversedZBuffer)
            {
                m.m20 = -m.m20;
                m.m21 = -m.m21;
                m.m22 = -m.m22;
                m.m23 = -m.m23;
            }

            var m2 = Matrix4x4.identity;
            m2.m00 = 0.5f;
            m2.m11 = 0.5f;
            m2.m22 = 0.5f;
            m2.m03 = 0.5f;
            m2.m23 = 0.5f;
            m2.m13 = 0.5f;

            var scale = 1f / split;
            var m3 = Matrix4x4.identity;
            m3.m00 = scale;
            m3.m11 = scale;
            m3.m03 = offset.x * scale;
            m3.m13 = offset.y * scale;

            return m3 * m2 * m;
        }

        private void SetCascadeData(int index, Vector4 cullingSphere, float tileSize)
        {
            var texelSize = 2f * cullingSphere.w / tileSize;
            var filterSize = texelSize * (1f + (float)_settings.shadowSettings.directional.filter);
            _cascadeData[index] = new Vector4(
                1f / cullingSphere.w,
                filterSize * 1.4142136f);
            cullingSphere.w -= filterSize;
            cullingSphere.w *= cullingSphere.w;
            _cascadeCullingSpheres[index] = cullingSphere;
        }

        private void SetKeywords(string[] keywords, int idx)
        {
            for (var i = 0; i < keywords.Length; i++)
            {
                if (i == idx)
                    _commandBuffer.EnableShaderKeyword(keywords[i]);
                else
                    _commandBuffer.DisableShaderKeyword(keywords[i]);
            }
        }

        public void Cleanup()
        {
            if (_shadowedDirectionalLightCount > 0)
            {
                _commandBuffer.ReleaseTemporaryRT(DirectionalShadowAtlasId);
                ExecuteCommandBuffer();
            }
        }

        private void ExecuteCommandBuffer()
        {
            _context.ExecuteCommandBuffer(_commandBuffer);
            _commandBuffer.Clear();
        }
    }
}