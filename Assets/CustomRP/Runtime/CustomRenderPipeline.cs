﻿using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public class CustomRenderPipeline : RenderPipeline
    {
        private CameraRenderer _cameraRenderer;

        public CustomRenderPipeline(CustomRenderPipelineSettings settings)
        {
            _cameraRenderer = new CameraRenderer(settings);
            GraphicsSettings.useScriptableRenderPipelineBatching = settings.useSRPBatcher;
            GraphicsSettings.lightsUseLinearIntensity = true;
        }

        protected override void Render(ScriptableRenderContext context, Camera[] cameras)
        {
            foreach (var camera in cameras)
                _cameraRenderer.Render(context, camera);
        }
    }
}