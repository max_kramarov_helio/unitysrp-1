using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public class Lighting
    {
        private const string BufferName = "Lighting";
        private static readonly int DirLightCountId = Shader.PropertyToID("_DirectionalLightCount");
        private static readonly int DirLightColorsId = Shader.PropertyToID("_DirectionalLightColors");
        private static readonly int DirLightDirectionsId  = Shader.PropertyToID("_DirectionalLightDirections");
        private static readonly int DirLightShadowDataId = Shader.PropertyToID("_DirectionalLightShadowData");
        private const int MaxDirLights = 4;
        private static readonly int OtherLightCountId = Shader.PropertyToID("_OtherLightCount");
        private static readonly int OtherLightColorsId = Shader.PropertyToID("_OtherLightColors");
        private static readonly int OtherLightPositionsId  = Shader.PropertyToID("_OtherLightPositions");
        private const int MaxOtherLights = 16;
        private static readonly string LightsPerObjectKeyword = "_LIGHTS_PER_OBJECT";

        private readonly CommandBuffer _commandBuffer;
        private readonly Vector4[] _dirLightColors;
        private readonly Vector4[] _dirLightDirections;
        private readonly Vector4[] _dirLightShadowData;
        private readonly Vector4[] _otherLightColors;
        private readonly Vector4[] _otherLightPositions;
        private readonly Shadows _shadows;
        private readonly CustomRenderPipelineSettings _settings;

        private ScriptableRenderContext _context;
        private CullingResults _cullingResults;

        public Lighting(Shadows shadows, CustomRenderPipelineSettings settings)
        {
            _commandBuffer = new CommandBuffer()
            {
                name = BufferName,
            };
            _dirLightColors = new Vector4[MaxDirLights];
            _dirLightDirections = new Vector4[MaxDirLights];
            _dirLightShadowData = new Vector4[MaxDirLights];
            _otherLightColors = new Vector4[MaxOtherLights];
            _otherLightPositions = new Vector4[MaxOtherLights];
            _shadows = shadows;
            _settings = settings;
        }

        public void Setup(ScriptableRenderContext context, CullingResults cullingResults)
        {
            _context = context;
            _cullingResults = cullingResults;

            _commandBuffer.BeginSample(BufferName);
            ExecuteCommandBuffer();

            SetupLight();

            _commandBuffer.EndSample(BufferName);
            ExecuteCommandBuffer();
        }

        private void SetupLight()
        {
            var visibleLights = _cullingResults.visibleLights;
            var dirLightCount = 0;
            var otherLightCount = 0;
            var indexMap = _settings.useLightsPerObject ?
                _cullingResults.GetLightIndexMap(Allocator.Temp) : default;
            var i = 0;
            for (i = 0; i < visibleLights.Length; i++)
            {
                var light = visibleLights[i];
                var newIdx = -1;
                switch (light.lightType)
                {
                    case LightType.Directional:
                        if (dirLightCount < MaxDirLights)
                        {
                            _dirLightColors[dirLightCount] = light.finalColor;
                            _dirLightDirections[dirLightCount] = -light.localToWorldMatrix.GetColumn(2);
                            _dirLightShadowData[dirLightCount] = _shadows.TryReserveDirectionalShadows(light.light, i);
                            dirLightCount++;
                        }
                        break;
                    case LightType.Point:
                        if (otherLightCount < MaxOtherLights)
                        {
                            _otherLightColors[otherLightCount] = light.finalColor;
                            var pos = light.localToWorldMatrix.GetColumn(3);
                            pos.w = 1f / Mathf.Max(light.range * light.range, 0.001f);
                            _otherLightPositions[otherLightCount] = pos;
                            newIdx = otherLightCount;
                            otherLightCount++;
                        }
                        break;
                }

                if (_settings.useLightsPerObject)
                {
                    indexMap[i] = newIdx;
                }
            }

            if (_settings.useLightsPerObject)
            {
                for (; i < indexMap.Length; i++)
                {
                    indexMap[i] = -1;
                }
                _cullingResults.SetLightIndexMap(indexMap);
                indexMap.Dispose();
                _commandBuffer.EnableShaderKeyword(LightsPerObjectKeyword);
            }
            else
            {
                _commandBuffer.DisableShaderKeyword(LightsPerObjectKeyword);
            }

            for (var j = dirLightCount; j < MaxDirLights; j++)
            {
                _dirLightColors[j] = Color.clear;
                _dirLightDirections[j] = new Vector4(0, 1, 0, 0);
            }
            for (var j = otherLightCount; j < MaxOtherLights; j++)
            {
                _otherLightColors[j] = Color.clear;
                _otherLightPositions[j] = new Vector4(0, 0, 0, 1);
            }

            _commandBuffer.SetGlobalInt(DirLightCountId, dirLightCount);
            if (dirLightCount > 0)
            {
                _commandBuffer.SetGlobalVectorArray(DirLightColorsId, _dirLightColors);
                _commandBuffer.SetGlobalVectorArray(DirLightDirectionsId, _dirLightDirections);
                _commandBuffer.SetGlobalVectorArray(DirLightShadowDataId, _dirLightShadowData);
            }

            _commandBuffer.SetGlobalInt(OtherLightCountId, otherLightCount);
            if (otherLightCount > 0)
            {
                _commandBuffer.SetGlobalVectorArray(OtherLightColorsId, _otherLightColors);
                _commandBuffer.SetGlobalVectorArray(OtherLightPositionsId, _otherLightPositions);
            }
        }

        private void ExecuteCommandBuffer()
        {
            _context.ExecuteCommandBuffer(_commandBuffer);
            _commandBuffer.Clear();
        }
    }
}