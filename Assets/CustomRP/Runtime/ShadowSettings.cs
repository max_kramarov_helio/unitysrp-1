using System;
using UnityEngine;

namespace CustomRP.Runtime
{
    public enum TextureSize
    {
        _256 = 256,
        _512 = 512,
        _1024 = 1024,
        _2048 = 2048,
        _4096 = 4096,
        _8192 = 8192,
    }

    public enum ShadowFilterMode
    {
        PCF2x2,
        PCF3x3,
        PCF5x5,
        PCF7x7,
    }

    public enum CascadeBlendMode
    {
        Hard,
        Soft,
        Dither,
    }

    [Serializable]
    public class ShadowSettings
    {
        [Serializable]
        public class Directional
        {
            public TextureSize textureSize;
            [Range(1, 4)] public int cascadeCount;
            [Range(0f, 1f)] public float cascadeRatio1;
            [Range(0f, 1f)] public float cascadeRatio2;
            [Range(0f, 1f)] public float cascadeRatio3;
            [Range(0.001f, 1f)] public float cascadeFade;
            public ShadowFilterMode filter;
            public CascadeBlendMode cascadeBlend;

            public Vector3 CascadeRatios => new Vector3(cascadeRatio1, cascadeRatio2, cascadeRatio3);
        }

        [Min(0.1f)]
        public float maxDistance = 100f;
        [Range(0.001f, 1f)]
        public float distanceFade = 0.1f;
        public Directional directional = new Directional()
        {
            textureSize = TextureSize._1024,
            cascadeCount = 4,
            cascadeRatio1 = 0.1f,
            cascadeRatio2 = 0.25f,
            cascadeRatio3 = 0.5f,
            cascadeFade = 0.1f,
            filter = ShadowFilterMode.PCF2x2,
            cascadeBlend = CascadeBlendMode.Hard,
        };
    }
}