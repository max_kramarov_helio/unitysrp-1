﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    [CreateAssetMenu]
    public class CustomRenderPipelineAsset : RenderPipelineAsset
    {
        [SerializeField] private bool _testRP;

        [Space]

        [SerializeField] private CustomRenderPipelineSettings _settings;

        protected override RenderPipeline CreatePipeline()
        {
            if (_testRP)
                return new TestRenderPipeline();

            return new CustomRenderPipeline(_settings);
        }
    }

    [Serializable]
    public class CustomRenderPipelineSettings
    {
        public bool useDynamicBatching;
        public bool useGPUInstancing;
        public bool useSRPBatcher;
        public ShadowSettings shadowSettings;
        public bool useLightsPerObject;
    }
}
