﻿#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED

#include "Surface.hlsl"
#include "Light.hlsl"
#include "BRDF.hlsl"
#include "GI.hlsl"

float3 GetIncomingLight(Surface surface, Light light)
{
    return saturate(dot(surface.normal, light.direction) * light.attenuation) * light.color;
}

float SpecularStrength(Surface surface, BRDF brdf, Light light)
{
    float3 h = SafeNormalize(light.direction + surface.viewDirection);
    float nh2 = Square(saturate(dot(surface.normal, h)));
    float lh2 = Square(saturate(dot(light.direction, h)));
    float r2 = Square(brdf.roughness);
    float d2 = Square(nh2 * (r2 - 1.0) + 1.00001);
    float normalization = brdf.roughness * 4.0 + 2.0;
    return r2 / (d2 * max(0.1, lh2) * normalization);
}

float3 DirectBRDF(Surface surface, BRDF brdf, Light light)
{
    return SpecularStrength(surface, brdf, light) * brdf.specular + brdf.diffuse;
}

float3 GetLighting(Surface surface, BRDF brdf, Light light)
{
    return GetIncomingLight(surface, light) * DirectBRDF(surface, brdf, light);
}

float3 GetLighting(Surface surface, BRDF brdf, GI gi)
{
    float3 color = gi.diffuse * brdf.diffuse;
    uint dirLightCount = GetDirectionalLightCount();
    ShadowData shadowData = GetShadowData(surface);
    for (uint i = 0; i < dirLightCount; i++)
    {
        Light light = GetDirectionLight(i, surface, shadowData);
        color += GetLighting(surface, brdf, light);
    }

#if defined(_LIGHTS_PER_OBJECT)
    uint otherLightCount = min(unity_LightData.y, 8);
    for (uint j = 0; j < otherLightCount; j++)
    {
        uint lightIdx = unity_LightIndices[j / 4][j % 4];
        Light light = GetOtherLight(lightIdx, surface, shadowData);
        color += GetLighting(surface, brdf, light);
    }
#else
    uint otherLightCount = GetOtherLightCount();
    for (uint j = 0; j < otherLightCount; j++)
    {
        Light light = GetOtherLight(j, surface, shadowData);
        color += GetLighting(surface, brdf, light);
    }
#endif

    return color;
}

#endif