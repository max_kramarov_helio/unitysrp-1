This project is a custom scriptable render pipeline for Unity3D. I followed the tutorial https://catlikecoding.com/unity/tutorials/custom-srp/.

# Scenes

## Scenes/BakedLight/BakedLight

It's a static geometry that gets GI from a lightmap:
![](ImagesForReadme/bakedLit_static.png)

It's a dynamic geometry that gets GI from a light probes:
![](ImagesForReadme/bakedLit_dynamic.png)

## Scenes/CustomShaders/CustomShaders

PBR shader:
![](ImagesForReadme/litShaders.png)

PBR shaders (GPU instancing):
![](ImagesForReadme/gpuInstancing.png)

## Scenes/PointLight/PointLight

Dynamic point lights:
![](ImagesForReadme/pointLights.png)

## Scenes/SampleScene/SampleScene - objects with build-in shaders and UI

Build-in lit shader (Standart), unlit shaders, rendering in texture, UI rendering:
![](ImagesForReadme/buildinShaders.png)

## Scenes/Shadows/Shadows - shadow casters

Dynamic shadows (4 casacades, soft cascades blend, pcf5x5 filtering)
![](ImagesForReadme/shadows.png)